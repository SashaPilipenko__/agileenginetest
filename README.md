# This is test task - AgileEngine

### `Run in development mode`

#### npm i, npm start

Runs the app in the development mode.
http://localhost:3000/

### `Deployment`

#### npm run build

Builds the app for production to the `build/` folder.
And you have all optimized and minifies files in `build/` folder.

You can see deployment project [Here](https://nostalgic-elion-a33309.netlify.app/) but api blocked it by cors
