import Axios from "axios";
import config from "_config";
import { setToken, getToken, AUTH_TOKEN } from "utils/auth";

const axios = Axios.create({
  baseURL: config.apiUrl,
});

axios.interceptors.request.use((req) => {
  const { headers } = req;

  const token = getToken();
  if (token) {
    headers.Authorization = `Bearer ${token}`;
  }

  return req;
});

axios.interceptors.response.use(
  (response) => response.data,
  async (err) => {
    const originalRequest = err.config;

    if (err.response.status === 401) {
      localStorage.removeItem(AUTH_TOKEN);
      const { token } = await axios
        .post("/auth", { apiKey: config.apiKey })
        .catch(() => console.error("cannot refresh token"));

      axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
      localStorage.setItem(AUTH_TOKEN, token);

      return axios(originalRequest);
    } else {
      return Promise.reject(err);
    }
  }
);

export default axios;
