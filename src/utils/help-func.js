export function getCoords(elem) {
  let item = elem.getBoundingClientRect();

  return {
    top: item.top + window.pageYOffset,
    left: item.left + window.pageXOffset,
  };
}
