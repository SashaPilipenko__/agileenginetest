import Api from "utils/api";
import config from "_config";
import { setToken } from "utils/auth";
import { ImagePreviewModel, ImageModel } from "models";

const { apiKey } = config;

export const startAuth = async () => {
  try {
    const { token } = await Api.post("/auth", { apiKey });
    setToken(token);
  } catch (e) {
    console.error(e);
  }
};

export const getImages = async (page) => {
  try {
    const { pictures, ...data } = await Api.get(`/images?page=${page + 1}`);
    return { ...data, images: pictures.map((i) => new ImagePreviewModel(i)) };
  } catch (e) {
    console.error(e);
  }
};

export const getSingleImage = async (id) => {
  try {
    const data = await Api.get(`/images/${id}`);
    return new ImageModel(data);
  } catch (e) {
    console.error(e);
  }
};
