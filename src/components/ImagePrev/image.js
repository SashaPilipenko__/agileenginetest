import React from "react";

import { ImageWrap } from "./styled";

function ImagePrev(props) {
  const { src, onOpen } = props;
  return (
    <div className="col-12 col-xs-12 col-sm-6 col-md-4">
      <ImageWrap onClick={onOpen}>
        <img className="img" src={src} alt="" />
      </ImageWrap>
    </div>
  );
}

export default ImagePrev;
