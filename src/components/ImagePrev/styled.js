import styled from "styled-components";

export const ImageWrap = styled.div`
  width: 100%;
  margin: 15px 0;
  cursor: pointer;
  min-height: 320px;
  .img {
    border-radius: 5px;
  }
`;
