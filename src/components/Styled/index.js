import styled from "styled-components";

export const PaginationWrap = styled.div`
  .pagination {
    display: flex;
    justify-content: center;
    overflow: hidden;
    li {
      border-radius: 4px;
      border: 1px solid lightblue;
      color: grey;
      background: white;
      cursor: pointer;
      &.active {
        color: lightgrey;
      }
      a {
        width: 42px;
        height: 42px;
        display: flex;
        align-items: center;
        justify-content: center;
      }
    }
  }
`;
