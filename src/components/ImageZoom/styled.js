import styled from "styled-components";

export const ImgWrap = styled.div`
  overflow: hidden;
  max-width: 100%;
`;

export const ImageStyle = styled.img`
  cursor: zoom-in;
  max-width: 100%;
`;
