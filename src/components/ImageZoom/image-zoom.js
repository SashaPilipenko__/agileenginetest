import React, { useEffect, useState } from "react";
import { getCoords } from "utils/help-func";

import { ImgWrap, ImageStyle } from "./styled";
import Image from "static/images/test.jpg";

function ImageZoom({ src }) {
  let [img, setImg] = useState(null);

  useEffect(() => {
    if (img) {
      img.ondragstart = () => {
        return false;
      };
    }
  }, [img]);

  const setRefImg = (node) => {
    setImg(node);
  };

  const onMouseDown = (e) => {
    const { shiftX, shiftY } = getCoordsToCursor(e);
    img.style.position = "absolute";
    img.style.transform = "scale(1.5)";
    img.style.cursor = "move";
    setPosition(e, shiftX, shiftY);

    document.onmousemove = (e) => {
      setPosition(e, shiftX, shiftY);
    };
  };

  const onMouseUp = () => {
    img.style.transform = "scale(1)";
    img.style.cursor = "zoom-in";
    img.style.position = "initial";
    img.style.left = "initial";
    img.style.top = "initial";
    document.onmousemove = null;
    img.onmouseup = null;
  };

  const getCoordsToCursor = (e) => {
    let coords = getCoords(img);
    let shiftX = e.clientX - coords.left;
    let shiftY = e.clientY - coords.top;

    return {
      shiftX,
      shiftY,
    };
  };

  const setPosition = (e, shiftX, shiftY) => {
    img.style.left = e.clientX - shiftX + "px";
    img.style.top = e.clientY - shiftY + "px";
  };

  return (
    <>
      <ImgWrap>
        <ImageStyle
          onMouseDown={onMouseDown}
          onMouseUp={onMouseUp}
          className="zoom"
          ref={setRefImg}
          src={src || Image}
          alt=""
        />
      </ImgWrap>
    </>
  );
}

export default ImageZoom;
