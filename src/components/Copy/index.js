import React from "react";
import copy from "copy-to-clipboard";

function CopyBlock({ data }) {
  const onCopy = () => {
    copy(data);
  };
  return (
    <button className="cursor-pointer" onClick={onCopy}>
      Share link
    </button>
  );
}

export default CopyBlock;
