import React, { useContext } from "react";
import { MainCtx } from "context";

import CopyBlock from "components/Copy";
import ImageZoom from "components/ImageZoom";
import {
  GlobalStyle,
  CloseButton,
  Overlay,
  LeftArrow,
  RightArrow,
  Modal,
} from "./styled";

const ImageModal = ({ setImage }) => {
  const {
    activeImage,
    activeImageVisible,
    closeActiveImage,
    prev,
    next,
  } = useContext(MainCtx);
  const { fullSrc, author, tags, camera } = activeImage;
  return (
    <>
      <GlobalStyle isOpen={activeImageVisible}> </GlobalStyle>
      <Modal isOpen={activeImageVisible}>
        <CloseButton onClick={closeActiveImage}>X</CloseButton>
        {prev && <LeftArrow onClick={setImage(prev)}>Prev</LeftArrow>}
        {next && <RightArrow onClick={setImage(next)}>Next</RightArrow>}

        <ImageZoom src={fullSrc} />

        <Overlay className="text-center">
          <h4 className="mb-3">{author}</h4>
          <CopyBlock data={fullSrc} />
          <div className="row align-items-center mt-3">
            <div className="col-12 col-md-6">
              <p>{camera}</p>
            </div>
            <div className="col-12 col-md-6">
              <p>{tags}</p>
            </div>
          </div>
        </Overlay>
      </Modal>
    </>
  );
};

export default ImageModal;
