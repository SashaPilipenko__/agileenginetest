import styled from "styled-components";
import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
  body {
    overflow: ${(props) => (props.isOpen ? "hidden" : "initial")};
  }
`;

export const CloseButton = styled.button`
  background: none;
  border: none;
  position: absolute;
  top: 3px;
  right: 5px;
  font-size: 24px;
  color: #fff;
`;

export const Overlay = styled.div`
  background: rgba(0, 0, 0, 0.5);
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  color: #fff;
  padding: 20px 60px;
`;

export const LeftArrow = styled.div`
  position: absolute;
  left: 40px;
  top: 50%;
  font-size: 20px;
  cursor: pointer;
  color: #fff;
  z-index: 9999;
`;

export const RightArrow = styled(LeftArrow)`
  left: initial;
  right: 40px;
`;

export const Modal = styled.div`
  position: fixed;
  top: 0;
  width: 100%;
  height: 100%;
  align-items: center;
  justify-content: center;
  background: #000;
  display: ${(props) => (props.isOpen ? "flex" : "none")};
`;
