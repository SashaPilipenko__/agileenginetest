export function ImagePreviewModel(obj) {
  return {
    id: obj.id,
    src: obj.cropped_picture,
  };
}

export function ImageModel(obj) {
  return {
    id: obj.id,
    src: obj.cropped_picture,
    camera: obj.camera,
    author: obj.author,
    fullSrc: obj.full_picture,
    tags: obj.tags,
  };
}
