import React, { useEffect, useContext } from "react";
import { MainCtx } from "context";
import { getImages, getSingleImage } from "api";
import ReactPaginate from "react-paginate";

import ImagePrev from "components/ImagePrev";
import ImageModal from "components/Modal";
import { PaginationWrap } from "components/Styled";

function Main(props) {
  const { page, pageCount, setImages, setActiveImage, images } = useContext(
    MainCtx
  );

  useEffect(() => {
    loadData({ selected: page });
  }, []);

  const loadData = async ({ selected }) => {
    const data = await getImages(selected);
    setImages(data);
  };

  const setImage = (id) => () => {
    getImage(id);
  };

  const getImage = async (id) => {
    const data = await getSingleImage(id);
    setActiveImage(data);
  };

  return (
    <>
      <div className="container container-fluid-xl">
        <h2 className="mt-4 text-center">Image gallery</h2>
        <div className="row">
          {images.map(({ id, ...item }) => (
            <ImagePrev key={id} {...item} onOpen={setImage(id)} />
          ))}
        </div>
        <PaginationWrap className="mt-5">
          <ReactPaginate
            previousLabel={"<"}
            nextLabel={">"}
            breakClassName={"break-me"}
            pageCount={pageCount}
            forcePage={page - 1}
            marginPagesDisplayed={2}
            pageRangeDisplayed={2}
            onPageChange={loadData}
            containerClassName={"pagination mb-4"}
            subContainerClassName={"pages pagination"}
            activeClassName={"active"}
          />
        </PaginationWrap>
      </div>
      <ImageModal setImage={setImage} />
    </>
  );
}

export default Main;
