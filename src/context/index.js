import React, { PureComponent } from "react";
export const MainCtx = React.createContext();

export class MainCtxProvider extends PureComponent {
  state = {
    page: 0,
    pageCount: 0,
    hasMore: true,
    loading: false,
    images: [],
    activeImage: {},
    activeImageVisible: false,
    next: "",
    prev: "",
  };

  setImages = (data) => {
    this.setState({
      ...data,
    });
  };

  setActiveImage = (data) => {
    const { images } = this.state;
    const idx = images.findIndex((i) => i.id === data.id);
    this.setState({
      activeImage: data,
      activeImageVisible: true,
      next: images[idx + 1]?.id,
      prev: images[idx - 1]?.id,
    });
  };

  closeActiveImage = () => {
    this.setState({
      activeImage: {},
      activeImageVisible: false,
    });
  };

  setLoading = (loading) => {
    this.setState({
      loading,
    });
  };

  render() {
    return (
      <MainCtx.Provider
        value={{
          ...this.state,
          setImages: this.setImages,
          setActiveImage: this.setActiveImage,
          closeActiveImage: this.closeActiveImage,
          setLoading: this.setLoading,
        }}
      >
        {this.props.children}
      </MainCtx.Provider>
    );
  }
}
