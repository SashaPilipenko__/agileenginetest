import React from "react";
import ReactDOM from "react-dom";
import AppRouter from "./app";

import "static/index.scss";

ReactDOM.render(
  <React.StrictMode>
    <AppRouter />
  </React.StrictMode>,
  document.getElementById("root")
);
