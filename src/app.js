import React, { useEffect } from "react";
import {
  Switch,
  Route,
  BrowserRouter as Router,
  Redirect,
} from "react-router-dom";
import { isAuth } from "utils/auth";
import { startAuth } from "api";

import { MainCtxProvider } from "context";
import Main from "containers/Main";

function AppRouter() {
  useEffect(() => {
    (async function () {
      // !isAuth() && (await startAuth());
    })();
  }, [isAuth]);
  return (
    <MainCtxProvider>
      <Router>
        <Switch>
          <Route exact path="/" component={Main} />
          <Route render={() => <Redirect to="/" />} />
        </Switch>
      </Router>
    </MainCtxProvider>
  );
}

export default AppRouter;
